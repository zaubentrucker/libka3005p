pub mod dummy;

use std::time::{Duration};
use serialport::{SerialPort, SerialPortInfo, SerialPortType};

const READ_BUFF_SIZE: usize = 128;

pub const REQUEST_WAIT_TIME_MS: Duration = Duration::from_millis(25);

// serialport::read() behaves differently on windows such that it always blocks until timeout
// while under unix the function returns as soon as data is available. Therefore a short timeout
// is needed on windows to achive high update rates.
#[cfg(target_os = "windows")]
pub const SERIAL_TIMEOUT: Duration = Duration::from_millis(10);
#[cfg(not(target_os = "windows"))]
pub const SERIAL_TIMEOUT: Duration = Duration::from_millis(1000);

/// How long we will keep polling on windows
pub const REQUEST_TIMEOUT: Duration = Duration::from_millis(1000);

pub const MAX_VOLTAGE_VOLT: f64 = 30.0;
pub const MAX_CURRENT_AMPERE: f64 = 5.0;

pub struct PowerSupply {
    serial: Box<dyn SerialPort>
}

#[derive(Debug)]
pub struct StatusReply {
    // only known if output is on. If so then 0 = CC-mode, 1 = CV-mode
    pub control_voltage: Option<bool>,
    pub beep: bool,
    pub over_current_protection: bool,
    pub over_voltage_protection: bool,
    pub output_on: bool,
}

enum StatusBits {
    ControlVoltage = 0,
    Beep = 4,
    OverCurrentProtection = 5,
    OverVoltageProtection = 7,
    OutputOn = 6,
}

#[derive(Debug)]
pub enum DeviceError {
    Fucked,
    ListPorts(String),
    CouldNotOpen(String),
    TestConnection,
    TimedOut,
    ConnectionLost,
    Unknown,
    IOError(String),
    ReplyParseError,
    ReadStatusReplyLength(usize),
    Write(std::io::Error),
    Read(std::io::Error),
}

pub fn list_serials() -> Result<Vec<String>, DeviceError> {
    let mut res = Vec::new();

    let ports = serialport::available_ports()
        .map_err(|e| DeviceError::ListPorts(e.description))?;
    let ports: Vec<&SerialPortInfo> = ports.iter().filter(|p| {
        match p.port_type {
            SerialPortType::UsbPort(_) => true,
            _ => false,
        }
    }).collect();

    for port in ports {
        res.push(port.port_name.clone());
    }

    Ok(res)
}

impl PowerSupply {

    pub fn read_set_values(&mut self) -> Result<(f64, f64), DeviceError> {
        let voltage = self.read_set_voltage()?;
        let current = self.read_set_current()?;

        Ok((voltage, current))
    }

    pub fn read_actual_values(&mut self) -> Result<(f64, f64), DeviceError> {
        let voltage = self.read_actual_voltage()?;
        let current = self.read_actual_current()?;

        Ok((voltage, current))
    }

    pub fn connect(path: &str, timeout: Duration) -> Result<PowerSupply, DeviceError> {
        let port = serialport::new(path, 9600);
        let port = port
            .data_bits(serialport::DataBits::Eight)
            .stop_bits(serialport::StopBits::One)
            .parity(serialport::Parity::None)
            .flow_control(serialport::FlowControl::Software)
            .timeout(timeout);

        Ok(PowerSupply {
            serial :port.open().map_err(|e| { DeviceError::CouldNotOpen(e.description) })?
        })
    }
    
    pub fn connect_dummy() -> PowerSupply {
        PowerSupply {
            serial: Box::new(dummy::DummySerial::new())
        }
    }

    fn read_string(&mut self) -> Result<String, DeviceError> {
        let reply = self.read_bytes()?;
        match String::from_utf8(reply) {
            Ok(utf8_str) => Ok(utf8_str),
            Err(_) => {
                return Err(DeviceError::ReplyParseError);
            }
        }
    }

    fn map_serial_error(err: std::io::Error) -> DeviceError {
        println!("err: {}", err);
        match err.kind() {
            std::io::ErrorKind::BrokenPipe => DeviceError::ConnectionLost,
            std::io::ErrorKind::TimedOut => DeviceError::TimedOut,
            _ => {
                panic!("Error not handled: {}", err);
            }
        }
    }
    
    /// Actually read from the serial port.
    ///
    /// This function is called by all other functions on the PowerSupply
    /// that expect a reply (mostly indirectly through `read_string()`.
    fn read_bytes(&mut self) -> Result<Vec<u8>, DeviceError> {
        let mut reply = Vec::new();

        // serialport has a bug on windows where initial reads time out
        // when performing rapid read cycles. This is the workaround:
        // -----------------
        if cfg!(target_os = "windows") {
            let start_time = std::time::Instant::now();
            loop {
                let mut buff: [u8; READ_BUFF_SIZE] = [0; READ_BUFF_SIZE];
                let bytes_read = match self.serial.read(&mut buff) {
                    Ok(x) => x,
                    Err(err) => {
                        if err.kind() == std::io::ErrorKind::TimedOut {
                            if start_time.elapsed() > REQUEST_TIMEOUT {
                                return Err(Self::map_serial_error(err));
                            }
                            else {
                                0
                            }
                        }
                        else {
                            return Err(Self::map_serial_error(err));
                        }
                    },
                };
                if bytes_read > 0 {
                    reply.append(&mut buff[..bytes_read].to_vec());
                    if reply.ends_with(b"\n") {
                        reply.remove(reply.len() - 1);
                        break;
                    }
                }
            }
        }
        // -----------------
        else {
            loop {
                let mut buff: [u8; READ_BUFF_SIZE] = [0; READ_BUFF_SIZE];
                let bytes_read = self.serial.read(&mut buff)
                    .map_err(Self::map_serial_error)?;
                if bytes_read > 0 {
                    reply.append(&mut buff[..bytes_read].to_vec());
                    if reply.ends_with(b"\n") {
                        reply.remove(reply.len() - 1);
                        break;
                    }
                }
            }
        }

        Ok(reply)
    }

    /// Actually write to the serial port.
    ///
    /// This function is called by all functions that send commands to the power supply.
    fn write_bytes(&mut self, message: Vec<u8>) -> Result<(), DeviceError> {
        let mut message = message.clone();
        message.push('\n' as u8);
        let mut written_count = 0;
        while written_count < message.len() {
            written_count = self.serial.write(&message[written_count..])
                .map_err(Self::map_serial_error)?;
        }
        Ok(())
    }
    
    fn read_with_request(&mut self, request: Vec<u8>) -> Result<String, DeviceError> {
        self.write_bytes( request)?;
        self.read_string()
    }

    pub fn read_status(&mut self) -> Result<StatusReply, DeviceError> {
        self.write_bytes( b"STATUS?".to_vec())?;
        let answer = self.read_bytes()?;
        if answer.len() != 1 {
            return Err(DeviceError::ReadStatusReplyLength(answer.len()));
        }

        let raw_value = answer[0];
        let extract_bool = |bit: StatusBits| {
            raw_value & 1 << (bit as u32) != 0
        };
        // we need to know whether we need the CC/CV bit first
        let output_on = extract_bool(StatusBits::OutputOn);
        let control_voltage = if output_on {
            Some(extract_bool(StatusBits::ControlVoltage))
        } else {
            None
        };

        Ok(StatusReply {
            control_voltage,
            beep: extract_bool(StatusBits::Beep),
            over_current_protection: extract_bool(StatusBits::OverCurrentProtection),
            over_voltage_protection: extract_bool(StatusBits::OverVoltageProtection),
            output_on,
        })
    }

    pub fn test_connection(&mut self) -> Result<String, DeviceError> {
        let message = b"*IDN?".to_vec();
        let answer = self.read_with_request(message)?;

        if !answer.to_lowercase().contains(&"KA3005P".to_lowercase()) {
            return Err(DeviceError::TestConnection);
        }
        Ok(answer)
    }

    pub fn read_set_voltage(&mut self) -> Result<f64, DeviceError> {
        let res = self.read_with_request(b"VSET1?".to_vec())?;
        let res: f64 = res.parse().map_err(|_| { DeviceError::ReplyParseError })?;
        Ok(res)
    }

    pub fn read_set_current(&mut self) -> Result<f64, DeviceError> {
        let res = self.read_with_request(b"ISET1?".to_vec())?;
        let res: f64 = res.parse().map_err(|_| { DeviceError::ReplyParseError })?;
        Ok(res)
    }

    pub fn read_actual_voltage(&mut self) -> Result<f64, DeviceError> {
        let res = self.read_with_request(b"VOUT1?".to_vec())?;
        let res: f64 = res.parse().map_err(|_| { DeviceError::ReplyParseError })?;
        Ok(res)
    }

    pub fn read_actual_current(&mut self) -> Result<f64, DeviceError> {
        let res = self.read_with_request(b"IOUT1?".to_vec())?;
        let res: f64 = res.parse().map_err(|_| { DeviceError::ReplyParseError })?;
        Ok(res)
    }

    pub fn write_voltage(&mut self, volt: f64) -> Result<(), DeviceError> {
        let message = format!("VSET1:{}", volt);
        self.write_bytes( message.as_bytes().to_vec())?;
        Ok(())
    }

    pub fn write_current(&mut self, ampere: f64) -> Result<(), DeviceError> {
        let message = format!("ISET1:{}", ampere);
        self.write_bytes( message.as_bytes().to_vec())?;
        Ok(())
    }

    pub fn write_output_state(&mut self, on: bool) -> Result<(), DeviceError> {
        let on = if on { 1 } else { 0 };
        let message = format!("OUT{}", on);
        self.write_bytes( message.as_bytes().to_vec())?;
        Ok(())
    }

    pub fn save_setting(&mut self, slot: u32) -> Result<(), DeviceError> {
        assert!(slot <= 5);
        let message = format!("SAV{}", slot);
        self.write_bytes( message.as_bytes().to_vec())?;
        Ok(())
    }

    pub fn write_over_current_protection(&mut self, on: bool) -> Result<(), DeviceError> {
        let on = if on { 1 } else { 0 };
        let message = format!("OCP{}", on);
        self.write_bytes(message.as_bytes().to_vec())?;
        Ok(())
    }

    pub fn write_over_voltage_protection(&mut self, on: bool) -> Result<(), DeviceError> {
        let on = if on { 1 } else { 0 };
        let message = format!("OVP{}", on);
        self.write_bytes(message.as_bytes().to_vec())?;
        Ok(())
    }
}
