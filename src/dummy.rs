//! # Dummy device for debugging and offline develompent
//! Provides a dummy [SerialPort](serialport::SerialPort) that acts as if there was a power supply
//! connected to it.
//! This is used for testing and developing the program without access to a physical ka3005p.


use std::cmp::{min};
use std::time::Duration;

use serialport::{StopBits, ClearBuffer, FlowControl, DataBits, Parity, Result, SerialPort};
use regex::Regex;

const BAUD_RATE: u64 = 9600;
const DELAY_PER_BYTE: Duration = Duration::from_micros(1000000 / BAUD_RATE);
const CONNECTED_RESISTANCE: f64 = 10.0;


/// Maps patterns for recognizing a specific type of set command to methods that handle the
/// command
const SET_PATTERNS: [(&str, fn(&mut DummySerial, &str)); 8] =
    [
        (r"\AOUT(0|1)\n\z", DummySerial::set_output),
        (r"\AVSET1:([[:digit:]]+\.?[[:digit:]]+)\n\z", DummySerial::set_voltage),
        (r"\AVSET1:([[:digit:]]+)\n\z", DummySerial::set_voltage),
        (r"\AISET1:([[:digit:]]+\.?[[:digit:]]+)\n\z", DummySerial::set_current),
        (r"\AISET1:([[:digit:]]+)\n\z", DummySerial::set_current),
        (r"\AOVP(0|1)\n\z", DummySerial::set_ovp),
        (r"\AOCP(0|1)\n\z", DummySerial::set_ocp),
        (r"\A\SAV([1-5])\n\z", DummySerial::save_pat),
    ];

/// Maps patterns for recognizing a specific type of get command to methods that handle the
/// command
const GET_PATTERNS: [(&str, fn(&mut DummySerial)); 6] =
    [
        (r"\AVSET1\?\n\z", DummySerial::get_set_voltage),
        (r"\AISET1\?\n\z", DummySerial::get_set_current),
        (r"\AVOUT1\?\n\z", DummySerial::get_actual_voltage),
        (r"\AIOUT1\?\n\z", DummySerial::get_actual_current),
        (r"\ASTATUS\?\n\z", DummySerial::get_status),
        (r"\A\*IDN\?\n\z", DummySerial::get_id),
    ];

/// Just contains the state of the virtual power supply.
/// It does not represent the BEEP and LOCK state as I won't be using them.
struct DummyState {
    output: bool,
    set_voltage: f64,
    set_current: f64,
    ovp: bool,
    ocp: bool,
}

/// Mockup SerialPort that imitates the behavior of a ka3005p power supply.
/// It behaves being set-up with both output clamps floating. The writing and reading delays due to
/// the limited baud rate are also imitated but any internal latencies within the power supply are
/// neglected for now. //TODO measure typical response times for accurate representation.
pub struct DummySerial {
    state: DummyState,
    input_buffer: Vec<u8>,
    output_buffer: Vec<u8>,
    get_regexps: Vec<(Regex, fn(&mut DummySerial))>,
    set_regexps: Vec<(Regex, fn(&mut DummySerial, &str))>,
}

impl DummySerial {
    pub fn new() -> DummySerial {
        let state = DummyState {
            output: false,
            set_voltage: 0.0,
            set_current: 0.0,
            ovp: false,
            ocp: false,
        };

        // The regex crate doesn't support compile time regexes anymore. I don't want to add
        // lazy_static as another dependency so I just init all regexes at runtime.
        fn init_regex<T>(val: (&str, T)) -> (Regex, T) {
            match val {
                (expr, func) => { (Regex::new(expr).unwrap(), func) }
            }
        }
        let set_regexps = SET_PATTERNS.iter().map(|x| { init_regex(*x) }).collect();
        let get_regexps = GET_PATTERNS.iter().map(|x| { init_regex(*x) }).collect();

        DummySerial {
            state,
            input_buffer: Vec::new(),
            output_buffer: Vec::new(),
            get_regexps,
            set_regexps,
        }
    }

    /// This method is called after a whole line has been written to the dummy
    fn handle_msg(&mut self, input: &str) {
        let mut handler = None;
        let mut value = None;

        // go through get_patterns
        for (regex, func) in &self.get_regexps {
            if regex.is_match(input) {
                handler = Some(func);
                break;
            }
        }
        if let Some(handler) = handler {
            handler(self);
            return;
        }

        // go through set_patterns
        // set handler has a different type than get handler so it has to be redefined
        let mut handler = None;
        for (regex, func) in &self.set_regexps {
            match regex.captures(input) {
                None => {}
                Some(captures) => {
                    handler = Some(func);
                    // All set patterns have a value
                    value = Some(captures.get(1).unwrap().as_str());
                    break;
                }
            }
        }
        if let Some(handler) = handler {
            // If a set_pattern was found, there should be a value
            handler(self, value.unwrap());
            return;
        }

        panic!("couldn't find a matching pattern for input: \"{}\"", input)
    }

    /// Used by set_ovp/ocp and set_output
    fn parse_01_to_bool(value: &str) -> std::result::Result<bool, ()> {
        let value: u8 = value.parse().map_err(|_| { () })?;
        match value {
            0 => Ok(false),
            1 => Ok(true),
            _ => Err(())
        }
    }

    /// Used by all get functions
    fn reply_value(&mut self, value: &str) {
        self.output_buffer.append(&mut value.as_bytes().to_vec());
        self.output_buffer.push(b'\n');
    }
}

impl DummySerial {
    fn set_voltage(&mut self, value: &str) {
        //TODO implement ovp
        let parsed_value = value.parse().expect(&format!("Couldn't parse set_voltage value: {}", value));
        let new_value = crate::MAX_VOLTAGE_VOLT.min(parsed_value);
        self.state.set_voltage = new_value;
    }
    fn set_current(&mut self, value: &str) {
        //TODO implement ocp
        let parsed_value = value.parse().expect(&format!("Couldn't parse set_voltage value: {}", value));
        let new_value = crate::MAX_CURRENT_AMPERE.min(parsed_value);
        self.state.set_current = new_value;
    }
    fn set_ovp(&mut self, value: &str) {
        let value = DummySerial::parse_01_to_bool(value)
            .expect(&format!("Couldn't parse \"{}\" as bool", value));
        self.state.ovp = value;
    }
    fn set_ocp(&mut self, value: &str) {
        let value = DummySerial::parse_01_to_bool(value)
            .expect(&format!("Couldn't parse \"{}\" as bool", value));
        self.state.ocp = value;
    }
    fn set_output(&mut self, value: &str) {
        let value = DummySerial::parse_01_to_bool(value)
            .expect(&format!("Couldn't parse \"{}\" as bool", value));
        self.state.output = value;
    }
    fn save_pat(&mut self, _value: &str) { /* I'm just going to ignore this */ }
    fn get_set_voltage(&mut self) {
        self.reply_value(&self.state.set_voltage.to_string())
    }
    fn get_set_current(&mut self) {
        self.reply_value(&self.state.set_current.to_string())
    }
    fn get_actual_voltage(&mut self) {
        let current_capped = self.state.set_current * CONNECTED_RESISTANCE;
        let capped_voltage = current_capped.min(self.state.set_voltage);
        let value = if self.state.output { capped_voltage } else { 0.0 };
        self.reply_value(&value.to_string())
    }
    fn get_actual_current(&mut self) {
        let voltage_capped = self.state.set_voltage / CONNECTED_RESISTANCE;
        let capped_current = voltage_capped.min(self.state.set_current);
        let value = if self.state.output { capped_current } else { 0.0 };
        self.reply_value(&value.to_string())
    }
    fn get_status(&mut self) {
        // it's always going to be current control since the virtual clamps are disconnected
        let mut res: u8 = 0b00000001;
        if self.state.ocp { res |= 0b00100000 }
        if self.state.ovp { res |= 0b10000000 }
        if self.state.output { res |= 0b01000000 }

        // Status byte is a raw value and handled differently to all other replies
        self.output_buffer.push(res);
        self.output_buffer.push(b'\n');
    }
    fn get_id(&mut self) {
        self.reply_value("RND 320-KA3005P V5.5 SN:39149786")
    }
}


impl std::io::Read for DummySerial {
    fn read(&mut self, buff: &mut [u8]) -> std::io::Result<usize> {
        let bytes_written = min(buff.len(), self.output_buffer.len());

        // simulate read speed of serial connection
        std::thread::sleep(DELAY_PER_BYTE.checked_mul(bytes_written as u32).unwrap());

        for i in 0..bytes_written {
            buff[i] = self.output_buffer.remove(0);
        }

        Ok(bytes_written)
    }
}

impl std::io::Write for DummySerial {
    fn write(&mut self, buff: &[u8]) -> std::io::Result<usize> {

        // simulate write speed of serial connection
        std::thread::sleep(DELAY_PER_BYTE.checked_mul(buff.len() as u32).unwrap());

        self.input_buffer.append(&mut buff.to_vec());
        let last = self.input_buffer.last();
        if let Some(char) = last {
            if *char == b'\n' {
                let input = self.input_buffer.clone();
                let input = std::str::from_utf8(&input)
                    .expect(&*format!("Gave non UTF8 to DummySerial:{:?}", self.input_buffer.clone()));
                self.input_buffer = Vec::new();
                self.handle_msg(input);
            }
        }
        Ok(buff.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        // nothing to do since there is no real file IO
        Ok(())
    }
}

impl serialport::SerialPort for DummySerial {
    fn name(&self) -> Option<String> {
        Some("Dummy".into())
    }

    // I'm not expecting to have to implement these as they are currently not used by the ka3005p
    // mod
    fn baud_rate(&self) -> Result<u32> { unimplemented!() }
    fn data_bits(&self) -> Result<DataBits> { unimplemented!() }
    fn flow_control(&self) -> Result<FlowControl> { unimplemented!() }
    fn parity(&self) -> Result<Parity> { unimplemented!() }
    fn stop_bits(&self) -> Result<StopBits> { unimplemented!() }
    fn timeout(&self) -> Duration { unimplemented!() }
    fn set_baud_rate(&mut self, _baud_rate: u32) -> Result<()> { unimplemented!() }
    fn set_data_bits(&mut self, _data_bits: DataBits) -> Result<()> { unimplemented!() }
    fn set_flow_control(&mut self, _flow_control: FlowControl) -> Result<()> { unimplemented!() }
    fn set_parity(&mut self, _parity: Parity) -> Result<()> { unimplemented!() }
    fn set_stop_bits(&mut self, _stop_bits: StopBits) -> Result<()> { unimplemented!() }
    fn set_timeout(&mut self, _timeout: Duration) -> Result<()> { unimplemented!() }
    fn write_request_to_send(&mut self, _level: bool) -> Result<()> { unimplemented!() }
    fn write_data_terminal_ready(&mut self, _level: bool) -> Result<()> { unimplemented!() }
    fn read_clear_to_send(&mut self) -> Result<bool> { unimplemented!() }
    fn read_data_set_ready(&mut self) -> Result<bool> { unimplemented!() }
    fn read_ring_indicator(&mut self) -> Result<bool> { unimplemented!() }
    fn read_carrier_detect(&mut self) -> Result<bool> { unimplemented!() }
    fn bytes_to_read(&self) -> Result<u32> { unimplemented!() }
    fn bytes_to_write(&self) -> Result<u32> { unimplemented!() }
    fn clear(&self, _buffer_to_clear: ClearBuffer) -> Result<()> { unimplemented!() }
    fn try_clone(&self) -> Result<Box<dyn SerialPort>> { unimplemented!() }
    fn set_break(&self) -> Result<()> { unimplemented!() }
    fn clear_break(&self) -> Result<()> { unimplemented!() }
}
